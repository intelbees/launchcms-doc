Installation
==========================

### Prerequisite
You need to install below packages before installing launchcms
* PHP 5.6
* MongoDB 3.2: all enterprise or community versions are good.
* MongoDB PHP Driver: [https://github.com/mongodb/mongo-php-driver]

```sh
$ pecl install mongodb
$ echo "extension=mongodb.so" >> `php --ini | grep "Loaded Configuration" | sed -e "s|.*:\s*||"`
```
### LaunchCMS setup
Do the git clone of launchcms
```sh
$ git clone https://gitlab.com/intelbees/launchcms.git launchcms
$ cd launchcms
```
Copy .env_example to .env file. Then, modify necessary configuration in .env file.

Then run below command to setup data structure
```sh
$ php artisan install
```

### LaunchCMS API setup

Do the git clone of launchcms-api

```sh
$ git clone https://gitlab.com/intelbees/launchcms-api.git launchcms-api
$ cd launchcms-api
```

Copy .env_example to .env file. Then, modify necessary configuration in .env file.

Then run below command to setup data structure
```sh
$ php artisan install
```

### Web server configuration

This section is applied for web server configuration on both LaunchCMS and LaunchCMS API

#### NGINX configuration

TBD

#### Apache configuration

TBD