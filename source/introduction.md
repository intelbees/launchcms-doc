Introduction
==========================================

LaunchCMS is a scalable headless CMS - which focuses on content management and provide a flexible data structure for other applications/modules to use.

LaunchCMS includes below sub projects: 

* launchcms: is the administration module for LaunchCMS. It provides the admin user interfaces to define and manage contents, users, user relationship
* launchcms-core: is the core module which contains all of business logic, model entities of the LaunchCMS. You can embedded it into your own Laravel project for using directly PHP service methods of the CMS. 
* launchcms-api: is the RESTful API module. It's built based on Lumen to provide APIs of LaunchCMS for other systems to use.
* launchcms-docker: this is just the docker machine configuration to run launchcms, launchcms-api in the integrated CI environment on Gitlab.

## Version
1.0.0

## Technologies

LaunchCMS uses below technologies to work properly:

* MongoDB 3.2 - the data storage of the whole system.
* Laravel 5.2 - awesome PHP web framework
* Redis - the recommend caching solution and queue processing of the appliaction.
* Elastic Search 2.0 - the full text search engine back end.



## Documentation

* [Administration guidelines](administration-guidelines.html)
* [Developer guidelines](developer-guidelines.html)

## Screenshots 

* Many useful field types to add into the content structure

![add-new-field](/images/add-new-field.png)

* Flexible way of defining meta data on fields (unique, index, full text search)

![field-meta-data](/images/field-meta-data.png)

* Support embedded list field where you can embedded a data type into the content type. By that way, it will create a very flexible structure. Below is the content entry screenshot for a use-case using embedded list fields: book contains records of contacts who borrow it.

![embedded-field-in-edit-content-screen](/images/embedded-field-in-edit-content-screen.png) 


[git-repo-url]: <https://gitlab.com/intelbees/launchcms.git>

