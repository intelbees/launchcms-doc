LaunchCMS - developer guidelines
================================

## Basic concepts

### Content type and field

Content type is the core concept of LaunchCMS. It's designed for modelling data structure of all business entities of your requirement, eg: Book, Author, Comment, Order ...

Each content type has a list of field. Field is the attribute  of the business entity, eg: title of a book, author of a book ...


### Field type - Field UI

As mentioned above, each content type has a list of fields. Each field has a specific type.

Currently, we're supporting below field types:

* String field
* Boolean field
* Integer field
* Double field
* Embedded field
* Embedded list field
* Reference field
* Reference list field
* Array field
* Date field
* Date time field
* Geo field

Each field has some different settings.  When create a new field on a specific content type, The UI that allows the developer to input field setting for a specific field type is called 'Field UI'.

Below is an example of a Field UI for the boolean field.

![field-ui-boolean](/images/boolean-field-ui.png)

## How to add a new field UI ?

Basically, above field types can adapt nearly 80-90 % your requirement for all content structure that you need. However, in some specific cases, you may need to add a new field type and make the Field UI for the developer to input settings for this field type.

In this section, we introduce how to add a new Field UI for a new field type that you want to support. 

Note: we will not cover "How to create a new field type" in this section. We will update that part in another topic.

### Step 1: Add a new field UI into config/launchcms.config

Inside the config/launchcms.config - you can see the section of:

```
'ui_fields' => [
        [
            'data_type' => 'string',
            'field_name_key' => 'launchcms.fields.string.name',
            'field_desc_key' => 'launchcms.fields.string.desc',
            'icon_class' => 'fa-header',
            'icon_text' => '',
            'field_dialog_class' => 'StringField'
        ],
        [
        	...
        ]
    ]

```

Each element inside this array is the configuration for a field type to be displayed in the below 'Add new field' dialog:

![add-new-field](/images/add-new-field.png)

* 'data_type' attribute is mapped to the alias of the field type.
* 'field_name_key' is the language key to display the Field name in the Field UI dialog.
* 'field_desc_key' is the language key to display the Field description in the Field UI dialog.
* 'icon_class' is the class to dispaly the icon of the field in the 'Add new field' dialog.
* 'icon_text' is used for the case that you want to use a short text (should be in max 4 characters) for the field icon in the 'Add new field' dialog.

Following this, you can add a new field UI to this setting.

### Step 2: Update the public/AdminTheme/dist/ui-field.js

As the Field UI is just a Bootstrap dialog model which displayed via the Javascript loading. The logic to display field UI is implemented in public/AdminTheme/dist/ui-field.js

In this file, basically we create different Javascript "module" for each UI field. Let's look at an example of the JS logic to handle the Geo field.

```js
//Below is the code following JS module pattern to handle the GeoField
launchcms.GeoFieldDialog = (function () {
    var dialog;
    var form;
    var module = {};
    var defaultDialogHandler;
    var loadFieldToForm = function(field) {
        // this line is used to load common meta data of the field (name, alias, settings ...)
        // into the field meta data tab.
        defaultDialogHandler.loadFieldMetaData(field);

        //Below this line, you can add more logic to handle loading your specific field settings if you want.

    };
    // Below method is used to show the dialog when user clicks into the field in Create new field dialog
    // OR click to an existing field to modify it.
    module.showDialog = function(field) {
        form[0].reset();
        if(field) {
            loadFieldToForm(field);
        }
        dialog.modal('show');
    };
    module.initialize = function() {
    	// You need to modify below selector depends on the ID of your Bootstrap modal HTML structure.
    	// Will mention about it later in the HTML structure of the field UI
        form = $('#geo-field-form');
        dialog = $('#geo-field-modal');
        defaultDialogHandler = new launchcms.FieldDialogHelper(form, dialog);
    };
    return module;
}());

...

$(function () {
    //below line is to initialize the GeoFieldDialog instance.
    launchcms.GeoFieldDialog.initialize();    
    //below line is to make the mapping of the field type alias with the dialog to trigger
    launchcms.Fields['geo'] = launchcms.GeoFieldDialog;
    ...
});

```


