Developer guideline
=====================

As LaunchCMS contains different modules - we decide to separate the developer guideline into different sections for each sub system.

* [LaunchCMS - developer guidelines](launchcms-developer-guidelines.html)
* [LaunchCMS API - developer guidelines](launchcms-api-developer-guidelines.html)